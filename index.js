// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Create server
const app = express();
const port = 4000;

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin@course-booking.oluqr.mongodb.net/course-booking-app?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
// Allow all resources to access our backend application.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Routes for our API
// localhost:4000/users
app.use("/users", userRoutes);
// localhost:4000/courses
app.use("/courses", courseRoutes);

// Listening to Port
// This syntax wil allow flexibility when using the application locally or as a hosted application (online).
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`)
})
